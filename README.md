##Instagram-Viewer

---

###This program utilizes the Instagram API to show photos

Photo feeds can be popular photos, photos with a certain tag, etc.  Features are still being added and tweaked.  Thumbnails of these photos are viewable on 127.0.0.1:8515 after starting the program.

API Keys are kept locally, to use you must generate your own API Key(s) and populate the code accordingly.

###End Goal

Ultimately, I hope to have this running on a Raspberry-Pi with an external monitor rotating through photos.  Similar to [this](https://github.com/samuelclay/Raspberry-Pi-Photo-Frame) example but using Instagram instead of Flickr and hopefully a larger screen.