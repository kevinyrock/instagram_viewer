import bottle
from instagramAPIKeys import CONFIG
from bottle import route, post, run, request
from instagram import client, subscriptions

api = client.InstagramAPI(**CONFIG)

@route('/')
def home():
    
    ####################################    
    #  Returns popular images
    ####################################    
    popular_media = api.media_popular(count=10)
    photos = []
    for media in popular_media:
        photos.append('<img src="%s"/>' % media.images['thumbnail'].url)
    return ''.join(photos)
        
        
    ####################################
    #  Returns images with a certain tag
    ####################################
    #tagged_media, next = api.tag_recent_media(count=10, tag_name="landscape")
    #photos = []
    #for media in tagged_media:
    #    photos.append('<img src="%s"/>' % media.images['thumbnail'].url)
    #return ''.join(photos)    
    
run(host='localhost', port=8515, reloader=True)